package cp.articlerep.ds;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author Ricardo Dias
 */
public class HashTable<K extends Comparable<K>, V> implements Map<K, V> {

	private static class Node {
		public Object key;
		public Object value;
		public Node next;

		public Node(Object key, Object value, Node next) {
			this.key = key;
			this.value = value;
			this.next = next;
		}
	}
	
	

	private ReentrantReadWriteLock[] locks;
	private AtomicInteger syncCounter = new AtomicInteger(0);
	private Node[] table;
	private Lock gets, unlocked;

	public HashTable() {
		this(1000);
		this.locks = new ReentrantReadWriteLock[1000];
		gets = new ReentrantLock(true);
		unlocked = new ReentrantLock(true);
	}

	public HashTable(int size) {
		this.table = new Node[size];
		this.locks = new ReentrantReadWriteLock[size];
		gets = new ReentrantLock(true);
		unlocked = new ReentrantLock(true);
		
		for(int i = 0; i<locks.length; i++)
		{
			locks[i] = new ReentrantReadWriteLock(true);
		}
	}

	private int calcTablePos(K key) {
		return Math.abs(key.hashCode()) % this.table.length;
	}

	
	
	@SuppressWarnings("unchecked")
	@Override
	public V put(K key, V value) {
		int pos = this.calcTablePos(key);
		Node n = this.table[pos];

		while (n != null && !n.key.equals(key)) {
			n = n.next;
		}

		if (n != null) {
			V oldValue = (V) n.value;
			n.value = value;
			return oldValue;
		}

		Node nn = new Node(key, value, this.table[pos]);
		this.table[pos] = nn;
		
		syncCounter.incrementAndGet();

		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public V remove(K key) {
		int pos = this.calcTablePos(key);
		Node p = this.table[pos];
		if (p == null) {
			return null;
		}

		if (p.key.equals(key)) {
			this.table[pos] = p.next;
			return (V) p.value;
		}

		Node n = p.next;
		while (n != null && !n.key.equals(key)) {
			p = n;
			n = n.next;
		}

		if (n == null) {
			return null;
		}

		p.next = n.next;

		syncCounter.decrementAndGet();

		return (V) n.value;
	}

	@SuppressWarnings("unchecked")
	@Override
	public V get(K key) {
		int pos = this.calcTablePos(key);
		Node n = this.table[pos];
		while (n != null && !n.key.equals(key)) {
			n = n.next;
		}
		return (V) (n != null ? n.value : null);
	}

	@Override
	public boolean contains(K key) {
		return get(key) != null;
	}

	/**
	 * No need to protect this method from concurrent interactions
	 */
	@Override
	public Iterator<V> values() {
		return new Iterator<V>() {

			private int pos = -1;
			private Node nextBucket = advanceToNextBucket();

			private Node advanceToNextBucket() {
				pos++;
				while (pos < HashTable.this.table.length
						&& HashTable.this.table[pos] == null) {
					pos++;
				}
				if (pos < HashTable.this.table.length)
					return HashTable.this.table[pos];

				return null;
			}

			@Override
			public boolean hasNext() {
				return nextBucket != null;
			}

			@SuppressWarnings("unchecked")
			@Override
			public V next() {
				V result = (V) nextBucket.value;

				nextBucket = nextBucket.next != null ? nextBucket.next
						: advanceToNextBucket();

				return result;
			}

			@Override
			public void rewind() {
				pos = -1;
				nextBucket = advanceToNextBucket();
			}

		};
	}

	@Override
	public Iterator<K> keys() {
		return null;
	}

	public int synchronizedSize()
	{
		return syncCounter.get();
	}

	public boolean validateMap()
	{
		return (table.length==synchronizedSize());
	}

	public void lockRead(K key)
	{
		int hashCode = calcTablePos(key);
		locks[hashCode].readLock().lock();
	}

	public void lockWrite(K key)
	{
		int hashCode = calcTablePos(key);
		locks[hashCode].writeLock().lock();
	}
	
	public void lockListWrite(List<K> key){
		gets.lock();
		Iterator<K> iterator = key.iterator();
		
		while(iterator.hasNext()){
			K lol = iterator.next();
			locks[calcTablePos(lol)].writeLock().lock();
		}
		
		gets.unlock();
	}
	
	public void lockListRead(List<K> key){
		gets.lock();
		Iterator<K> iterator = key.iterator();
		
		while(iterator.hasNext()){
			K lol = iterator.next();
			locks[calcTablePos(lol)].readLock().lock();
		}
		
		gets.unlock();
	}
	
	
	
	
	public void unlockRead(K key)
	{
		int hashCode = calcTablePos(key);
		locks[hashCode].readLock().unlock();
	}

	public void unlockWrite(K key)
	{
		int hashCode = calcTablePos(key);
		locks[hashCode].writeLock().unlock();
	}
	
	public void unlockListWrite(List<K> key){
		unlocked.lock();
		Iterator<K> iterator = key.iterator();
		
		while(iterator.hasNext()){
			K lol = iterator.next();
			locks[calcTablePos(lol)].writeLock().unlock();
		}
		unlocked.unlock();
	}
	
	public void unlockListRead(List<K> key){
		//unlocked.lock();
		Iterator<K> iterator = key.iterator();
		
		while(iterator.hasNext()){
			K lol = iterator.next();
			locks[calcTablePos(lol)].readLock().unlock();
		}
		
		//unlocked.unlock();
	}

	
}
