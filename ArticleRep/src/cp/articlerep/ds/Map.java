package cp.articlerep.ds;

import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author Ricardo Dias
 */
public interface Map<K extends Comparable<K>, V> {
	public V put(K key, V value);
	public boolean contains(K key);
	public V remove(K key);
	public V get(K key);
	
	public Iterator<V> values();
	public Iterator<K> keys();

	public boolean validateMap();
	public void lockWrite(K key);
	public void lockRead(K key);
	public void lockListWrite(List<K> key);
	public void lockListRead(List<K> key);
	public void unlockWrite(K key);
	public void unlockRead(K key);
	public void unlockListWrite(List<K> key);
	public void unlockListRead(List<K> key);
}
