package cp.articlerep;

import java.util.HashSet;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import cp.articlerep.ds.Iterator;
import cp.articlerep.ds.LinkedList;
import cp.articlerep.ds.List;
import cp.articlerep.ds.Map;
import cp.articlerep.ds.HashTable;

/**
 * @author Ricardo Dias
 */
public class Repository {

	private Map<String, List<Article>> byAuthor;
	private Map<String, List<Article>> byKeyword;
	private Map<Integer, Article> byArticleId;
	private AtomicInteger numAdds = new AtomicInteger(0);
	private AtomicInteger numRems = new AtomicInteger(0);

	public Repository(int nkeys) {
		this.byAuthor = new HashTable<String, List<Article>>(40000);
		this.byKeyword = new HashTable<String, List<Article>>(40000);
		this.byArticleId = new HashTable<Integer, Article>(40000);
	}
	
	

	public boolean insertArticle(Article a) {

		byArticleId.lockRead(a.getId());
		if (byArticleId.contains(a.getId())){
			byArticleId.unlockRead(a.getId());
			return false;
		}
		byArticleId.unlockRead(a.getId());
			
		try{
			byArticleId.lockWrite(a.getId());
			byAuthor.lockListWrite(a.getAuthors());
			byKeyword.lockListWrite(a.getKeywords());
			
			byArticleId.put(a.getId(), a);
			
			Iterator<String> authors = a.getAuthors().iterator();
			while (authors.hasNext()) {
				String name = authors.next();

				List<Article> ll = byAuthor.get(name);
				if (ll == null) {
					ll = new LinkedList<Article>();
					byAuthor.put(name, ll);
				}
				ll.add(a);
			}
			
			Iterator<String> keywords = a.getKeywords().iterator();
			while (keywords.hasNext()) {
				String keyword = keywords.next();

				List<Article> ll = byKeyword.get(keyword);
				if (ll == null) {
					ll = new LinkedList<Article>();
					byKeyword.put(keyword, ll);
				} 
				ll.add(a);
			}

			numAdds.incrementAndGet();
			return true;
			
		} finally {
			byArticleId.unlockWrite(a.getId());
			byAuthor.unlockListWrite(a.getAuthors());
			byKeyword.unlockListWrite(a.getKeywords());
		}
		
	}

	public void removeArticle(int id) {
		byArticleId.lockRead(id);
		Article a = byArticleId.get(id);
		byArticleId.unlockRead(id);
		
		if (a == null)
			return;

		try{
			byArticleId.lockWrite(id);
			byAuthor.lockListWrite(a.getAuthors());
			byKeyword.lockListWrite(a.getKeywords());
			
			Iterator<String> keywords = a.getKeywords().iterator();
			while (keywords.hasNext()) {
				String keyword = keywords.next();

				List<Article> ll = byKeyword.get(keyword);
				if (ll != null) {

					int pos = 0;
					Iterator<Article> it = ll.iterator();
					while (it.hasNext()) {
						Article toRem = it.next();
						if (toRem == a) {
							break;
						}
						pos++;
					}
					ll.remove(pos);
					it = ll.iterator();
					if (!it.hasNext()) { // checks if the list is empty
						byKeyword.remove(keyword);
					}
				}
			}

			Iterator<String> authors = a.getAuthors().iterator();
			while (authors.hasNext()) {
				String name = authors.next();

				List<Article> ll = byAuthor.get(name);
				if (ll != null) {
					int pos = 0;
					Iterator<Article> it = ll.iterator();
					while (it.hasNext()) {
						Article toRem = it.next();
						if (toRem == a) {
							break;
						}
						pos++;
					}
					ll.remove(pos);
					it = ll.iterator(); 
					if (!it.hasNext()) { // checks if the list is empty
						byAuthor.remove(name);
					}
				}
			}

			numRems.decrementAndGet();
			byArticleId.remove(id);
		} finally{
			byArticleId.unlockWrite(id);
			byAuthor.unlockListWrite(a.getAuthors());
			byKeyword.unlockListWrite(a.getKeywords());
		}
		
		
	}

	public List<Article> findArticleByAuthor(List<String> authors) {
		List<Article> res = new LinkedList<Article>();

		try{
			byAuthor.lockListRead(authors);
			
			Iterator<String> it = authors.iterator();
			while (it.hasNext()) {
				String name = it.next();
				List<Article> as = byAuthor.get(name);
				if (as != null) {
					Iterator<Article> ait = as.iterator();
					while (ait.hasNext()) {
						Article a = ait.next();
						res.add(a);
					}
				}
			}

			return res;
	} finally{
		byAuthor.unlockListRead(authors);
	}
			
	}

	public List<Article> findArticleByKeyword(List<String> keywords) {
		List<Article> res = new LinkedList<Article>();
		
		try{
			byKeyword.lockListRead(keywords);
		
			Iterator<String> it = keywords.iterator();
			while (it.hasNext()) {
				String keyword = it.next();
				List<Article> as = byKeyword.get(keyword);
				if (as != null) {
					Iterator<Article> ait = as.iterator();
					while (ait.hasNext()) {
						Article a = ait.next();
						res.add(a);
					}
				}
			}

			return res;
			
		} finally{
			byKeyword.unlockListRead(keywords);
		}
		
	}

	
	/**
	 * This method is supposed to be executed with no concurrent thread
	 * accessing the repository.
	 * 
	 */
	public boolean validate() {
		
		HashSet<Integer> articleIds = new HashSet<Integer>();
		HashSet<String> authors = new HashSet<String>();
		HashSet<String> keywords = new HashSet<String>();
		
		int articleCount = 0;
		
		
		Iterator<Article> aIt = byArticleId.values();
		while(aIt.hasNext()) {
			Article a = aIt.next();
			
			articleIds.add(a.getId());
			articleCount++;
			
			// check the authors consistency
			Iterator<String> authIt = a.getAuthors().iterator();
			while(authIt.hasNext()) {
				String name = authIt.next();
				if (!searchAuthorArticle(a, name)) {
					return false;
				}
				authors.add(name);
			}
			
			// check the keywords consistency
			Iterator<String> keyIt = a.getKeywords().iterator();
			while(keyIt.hasNext()) {
				String keyword = keyIt.next();
				if (!searchKeywordArticle(a, keyword)) {
					return false;
				}
				keywords.add(keyword);
			}
		}		
		
		return (articleCount == articleIds.size()) && (authors.size() == sizeAuthors()) && (keywords.size() == sizeKeys()) ;
	}
	
	private int sizeAuthors(){
		int size = 0;
		Iterator<List<Article>> itAut = byAuthor.values();
		while(itAut.hasNext()){
			itAut.next();
			size++;
		}
		
		return size;
	}
	
	private int sizeKeys(){
		int size = 0;
		Iterator<List<Article>> itKey = byKeyword.values();
		while(itKey.hasNext()){
			itKey.next();
			size++;
		}
		
		return size;
	}
	
	private boolean searchAuthorArticle(Article a, String author) {
		List<Article> ll = byAuthor.get(author);
		if (ll != null) {
			Iterator<Article> it = ll.iterator();
			while (it.hasNext()) {
				if (it.next() == a) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean searchKeywordArticle(Article a, String keyword) {
		List<Article> ll = byKeyword.get(keyword);
		if (ll != null) {
			Iterator<Article> it = ll.iterator();
			while (it.hasNext()) {
				if (it.next() == a) {
					return true;
				}
			}
		}
		return false;
	}

}
